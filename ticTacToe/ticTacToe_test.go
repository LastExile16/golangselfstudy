package main

import "testing"

func Test01(t *testing.T) {
	b := &Board{
		tokens: []int{0, 0, 0, 0, 0, 0, 0, 0, 0},
	}
	b.put(2, 1, "o")
	if b.get(2, 1) != "o" {
		t.Errorf("put function error")
	}
}

func Test02(t *testing.T) {
	b := &Board{
		tokens: []int{1, 0, 0, 1, 0, 0, 1, 0, 0},
	}
	if b.gameState(0, 3, 6) != true {
		t.Errorf("The test case failed")
	}
}

func Test03(t *testing.T) {
	b := &Board{
		tokens: []int{1, 0, 0, 0, 1, 0, 0, 0, 1},
	}
	if b.check() != true {
		t.Errorf("check function error")
	}
}
